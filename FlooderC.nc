/*

Copyright © Andrea Turri, Andrea Salmoiraghi 2016
FlooderC.nc 15/02/2016

*/

#include "Flooder.h"

module FlooderC {
  uses interface Boot;
  uses interface Timer<TMilli> as Timer0;
  uses interface Packet;
  uses interface AMPacket;
  uses interface AMSend;
  uses interface Receive;
  uses interface SplitControl as AMControl;
}
implementation {
  uint8_t counter;
  uint8_t counter_lastForwarded;
  message_t pkt;
  bool busy = FALSE;

  //For simulation only, to gather summarized data
  uint16_t count_uniquePackets;
  uint16_t count_recvPackets;
  uint16_t count_fwdPackets;

  event void Boot.booted() {
    dbg("default","%s | Node %d started\n", sim_time_string(), TOS_NODE_ID);
    call AMControl.start();
  }

  event void AMControl.startDone(error_t err) {
    if (err == SUCCESS) {
      if(TOS_NODE_ID==0) call Timer0.startPeriodic(TIMER_PERIOD);
    } else {
      call AMControl.start();
    }
  }

  event void AMControl.stopDone(error_t err) {}

  event void Timer0.fired() {
    counter++;
    if (!busy) {
      FlooderMsg* btrpkt = (FlooderMsg*)(call Packet.getPayload(&pkt, sizeof(FlooderMsg)));
      if (btrpkt == NULL) return;
      btrpkt->nodeid = TOS_NODE_ID;
      btrpkt->counter = counter;
      if (call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(FlooderMsg)) == SUCCESS) {
        //Update last forwarded packet couneter
        counter_lastForwarded = btrpkt->counter;
	      busy = TRUE;
	      count_uniquePackets++;
        dbg("default","%s | Sent counter=%d from %d\n", sim_time_string(), counter, TOS_NODE_ID);
      }
    }
  }

  event void AMSend.sendDone(message_t* msg, error_t err) {
    if (&pkt == msg) busy = FALSE;
  }

  event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len){
    am_addr_t sourceAddr;
    if (len == sizeof(FlooderMsg)) {

      FlooderMsg* btrpkt = (FlooderMsg*)payload;
      sourceAddr = call AMPacket.source(msg);
      dbg("default","%s | Received packet %d from %d (originally from %d)\n",sim_time_string(),btrpkt->counter, sourceAddr, btrpkt->nodeid);

      //Avoid re-forwarding duplicates...
      if(counter_lastForwarded >= btrpkt->counter) {
        //If the last packet forwarded has the counter >= the currently received packet counter, it means that it is duplicate
        dbg("default","%s | Packet %d is dup. (last was %d), not re-fwing\n",sim_time_string(),btrpkt->counter,counter_lastForwarded);
        return msg;
      }

      count_recvPackets++;
      //If instead the packet is not duplicate, forward it...
      if (!busy) {
        FlooderMsg* btrpkt1 = (FlooderMsg*)(call Packet.getPayload(&pkt,sizeof(FlooderMsg)));
        if (btrpkt1 == NULL) return msg;
        btrpkt1->nodeid = btrpkt->nodeid;
        btrpkt1->counter = btrpkt->counter;
        if (call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(FlooderMsg)) == SUCCESS) {
           //Update last forwarded packet couneter
           counter_lastForwarded = btrpkt1->counter;
           busy = TRUE;
	   count_fwdPackets++;
           dbg("default","%s | Forwarding packet %d\n", sim_time_string(),btrpkt->counter);
        }
      }

    }

    return msg;
  }
}
