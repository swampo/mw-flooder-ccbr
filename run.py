#!/usr/bin/python

N_MOTES = 256
SIM_TIME = 10800 #sec
TOPO_FILE = "linkgain.out"
#SIM_MODULE = "FlooderC"
SIM_MODULE = "CCBRC"
NOISE_FILE = "/usr/src/tinyos/tos/lib/tossim/noise/meyer-heavy.txt" #cut to 380 lines only
DBG_CHANNELS = "default error"

from TOSSIM import *
from tinyos.tossim.TossimApp import *
from random import *
import sys

n = NescApp()
t = Tossim(n.variables.variables())
r = t.radio()

t.randomSeed(1)

for channel in DBG_CHANNELS.split():
    t.addChannel(channel, sys.stdout)


#add gain links
f = open(TOPO_FILE, "r")
lines = f.readlines()

for line in lines:
    s = line.split()
    if (len(s) > 0):
        if s[0] == "gain":
            r.add(int(s[1]), int(s[2]), float(s[3]))
        elif s[0] == "noise":
            r.setNoise(int(s[1]), float(s[2]), float(s[3]))
	
#add noise trace
noise = open(NOISE_FILE, "r")
lines = noise.readlines()
for line in lines:
    str = line.strip()
    if (str != ""):
        val = int(float(str))
        for i in range(0, N_MOTES):
            t.getNode(i).addNoiseTraceReading(val)

from random import randint
#init motes
for i in range (0, N_MOTES):
    time=i * t.ticksPerSecond() / 10
    time=i
    m=t.getNode(i)
    m.bootAtTime(time)
    m.createNoiseModel()
    print "Booting ", i, " at ~ ", time, "s"


time = t.time()
lastTime =  t.ticksPerSecond() * 60 #every 60s to read better the log
while (time + SIM_TIME * t.ticksPerSecond() > t.time()):
    if(t.time() > lastTime):
        lastTime = lastTime + t.ticksPerSecond() * 60
        print "----------------------------------SIMULATION: ~", t.time()/t.ticksPerSecond(), " s ----------------------"
    t.runNextEvent()
print "----------------------------------END OF SIMULATION-------------------------------------"

print "\n\nSummarized data:"

#sink
m=t.getNode(0)
v=m.getVariable(SIM_MODULE+".count_uniquePackets")
count_uniquePackets=v.getData()
print "\n\n# sent packets from sink: ", count_uniquePackets ,"\n\n"

# other motes than the sink
# % of received packets (w/o counting duplicates) and average
total=0.0
for i in range (1,N_MOTES):
	m=t.getNode(i)
	v=m.getVariable(SIM_MODULE+".count_recvPackets")
	count_recvPackets=v.getData()
	local_tot = float(count_recvPackets)/float(count_uniquePackets)*float(100)
	total=float(total)+float(local_tot)
	print "[Node ",i,"] % of received packets: ", local_tot, "(", count_recvPackets ," packets of ",count_uniquePackets,")"

avg=total/(N_MOTES-1)
print "\n\n"
# other motes than the sink
# number of forwarded packets
total=0
for i in range (1,N_MOTES):
	m=t.getNode(i)
	v=m.getVariable(SIM_MODULE+".count_fwdPackets")
	count_fwdPackets=v.getData()
	total=total+count_fwdPackets
	print "[Node ",i,"] # of forwarded packets: ", count_fwdPackets

print "\n\n% unique packets received (avg): ", avg,"\nTotal forwarded packets: ", total,"\n\n"
