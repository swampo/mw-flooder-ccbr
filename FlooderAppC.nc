/*

Copyright © Andrea Turri, Andrea Salmoiraghi 2016
FlooderAppC.nc 15/02/2016

*/

#include "Flooder.h"

configuration FlooderAppC { }
implementation {
  components MainC;
  components FlooderC as App;
  components new TimerMilliC() as Timer0;
  components ActiveMessageC;
  components new AMSenderC(AM_FLOODER);
  components new AMReceiverC(AM_FLOODER);

  App.Boot -> MainC;
  App.Timer0 -> Timer0;
  App.Packet -> AMSenderC;
  App.AMPacket -> AMSenderC;
  App.AMControl -> ActiveMessageC;
  App.AMSend -> AMSenderC;
  App.Receive -> AMReceiverC;
}
