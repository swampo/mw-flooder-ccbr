/*

Copyright © Andrea Turri, Andrea Salmoiraghi 2016
CCBRC.nc 15/02/2016

*/

#include "CCBR.h"

module CCBRC {
  uses interface Boot;
  uses interface Timer<TMilli> as Timer0;
  uses interface Timer<TMilli> as Timer1;
  uses interface Random;
  uses interface Packet;
  uses interface AMPacket;
  uses interface AMSend;
  uses interface Receive;
  uses interface SplitControl as AMControl;
}
implementation {
  uint8_t counter;
  uint8_t counter_lastForwarded;
  message_t pkt;
  uint32_t delay;
  bool busy = FALSE;
  CCBRMsg* msgWaiting;

  //For simulation only, to gather summarized data
  uint16_t count_uniquePackets;
  uint16_t count_recvPackets;
  uint16_t count_fwdPackets;

  event void Boot.booted() {
    dbg("default","%s | Node %d started\n", sim_time_string(), TOS_NODE_ID);
    call AMControl.start();
  }

  event void AMControl.startDone(error_t err) {
    if (err == SUCCESS) {
      if(TOS_NODE_ID==0) call Timer0.startPeriodic(TIMER_PERIOD);
      msgWaiting = (CCBRMsg*)(call Packet.getPayload(&pkt, sizeof(CCBRMsg)));
      msgWaiting->nodeid = -1;
      msgWaiting->counter = -1;
    } else {
      call AMControl.start();
    }
  }

  event void AMControl.stopDone(error_t err) {}

  event void Timer0.fired() {
    counter++;
    if (!busy) {
      CCBRMsg* btrpkt = (CCBRMsg*)(call Packet.getPayload(&pkt, sizeof(CCBRMsg)));
      if (btrpkt == NULL) return;
      btrpkt->nodeid = TOS_NODE_ID;
      btrpkt->counter = counter;
      if (call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(CCBRMsg)) == SUCCESS) {
        //Update last forwarded packet couneter
        counter_lastForwarded = btrpkt->counter;
	      busy = TRUE;
	      count_uniquePackets++;
        dbg("default","%s | Sent counter=%d from %d\n", sim_time_string(), counter, TOS_NODE_ID);
      }
    }
  }

  event void Timer1.fired() {
    if (!busy) {
      CCBRMsg* btrpkt = (CCBRMsg*)(call Packet.getPayload(&pkt,sizeof(CCBRMsg)));
      if (btrpkt == NULL) return;
      btrpkt->nodeid = msgWaiting->nodeid;
      btrpkt->counter = msgWaiting->counter;
      if (call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(CCBRMsg)) == SUCCESS) {
         //Update last forwarded packet couneter
         counter_lastForwarded = btrpkt->counter;
         busy = TRUE;
	 count_fwdPackets++;
         dbg("default","%s | Forwarding packet %d\n", sim_time_string(),btrpkt->counter);
      }
    }
  }

  event void AMSend.sendDone(message_t* msg, error_t err) {
    if (&pkt == msg) busy = FALSE;
  }

  event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len){
    am_addr_t sourceAddr;
    if (len == sizeof(CCBRMsg)) {

      CCBRMsg* btrpkt = (CCBRMsg*)payload;
      sourceAddr = call AMPacket.source(msg);
      dbg("default","%s | Received packet %d from %d (originally from %d)\n",sim_time_string(),btrpkt->counter, sourceAddr, btrpkt->nodeid);

      //If the message is the one for which the timer has been set, stop it!
      if(msgWaiting->counter == btrpkt->counter && call Timer1.isRunning()) {
        call Timer1.stop();
        dbg("default","%s | Heard packet %d! Stop waiting for this packet\n",sim_time_string(),btrpkt->counter);
      }

      //Avoid re-forwarding duplicates...
      if(counter_lastForwarded >= btrpkt->counter) {
        dbg("default","%s | Packet %d is dup. (last was %d), not re-fwing\n",sim_time_string(),btrpkt->counter,counter_lastForwarded);
        return msg;
      } else if(msgWaiting->counter == btrpkt->counter) {
	return msg;
      }

      count_recvPackets++;
      //If instead the packet is not duplicate, remember it and wait some seconds before forwarding it...
      msgWaiting->nodeid = btrpkt->nodeid;
      msgWaiting->counter = btrpkt->counter;
      //Setup random delay between 0 and MAX_WAITs to wait before re-forwarding it
      delay = call Random.rand32()%MAX_WAIT;
      call Timer1.startOneShot(delay);
      dbg("default","%s | Waiting %d ms to hear again the packet %d...\n",sim_time_string(),delay,btrpkt->counter);

    }

    return msg;
  }
}
