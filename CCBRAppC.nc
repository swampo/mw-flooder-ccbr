/*

Copyright © Andrea Turri, Andrea Salmoiraghi 2016
CCBRAppC.nc 15/02/2016

*/

#include "CCBR.h"

configuration CCBRAppC { }
implementation {
  components MainC;
  components CCBRC as App;
  components new TimerMilliC() as Timer0;
  components new TimerMilliC() as Timer1;
  components RandomC;
  components ActiveMessageC;
  components new AMSenderC(AM_CCBR);
  components new AMReceiverC(AM_CCBR);

  App.Boot -> MainC;
  App.Timer0 -> Timer0;
  App.Timer1 -> Timer1;
  App.Random -> RandomC;
  App.Packet -> AMSenderC;
  App.AMPacket -> AMSenderC;
  App.AMControl -> ActiveMessageC;
  App.AMSend -> AMSenderC;
  App.Receive -> AMReceiverC;
}
