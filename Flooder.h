/*

Copyright © Andrea Turri, Andrea Salmoiraghi 2016
Flooder.h 15/02/2016

*/

#ifndef FLOODER_H
#define FLOODER_H

enum {
  AM_FLOODER = 6,
  TIMER_PERIOD = 1024*60
};

typedef nx_struct FlooderMsg {
  nx_uint8_t nodeid;
  nx_uint8_t counter;
  nx_uint8_t pad[18];
} FlooderMsg;

#endif
