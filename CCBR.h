/*

Copyright © Andrea Turri, Andrea Salmoiraghi 2016
CCBR.h 15/02/2016

*/

#ifndef CCBR_H
#define CCBR_H

enum {
  AM_CCBR = 6,
  TIMER_PERIOD = 1024*60,
  MAX_WAIT = 1024*11
};

typedef nx_struct CCBRMsg {
  nx_uint8_t nodeid;
  nx_uint8_t counter;
  nx_uint8_t pad[18];
} CCBRMsg;

#endif
