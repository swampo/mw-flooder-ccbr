Andrea Turri, Andrea Salmoiraghi

Middleware Technologies for Distributed Systems

TinyOS Flooding & CCBR-like simulations

The sink of a WSN have to periodically (every 60s) distribute some data to all the other nodes. Implement a flooding protocol to distribute such data. Implement two version of the flooding protocol. A trivial version in which each node simply reforward the packet it receives, and a more efficient version (similar to the CCBR protocol we studied) in which each node waits for a random delay before reforwarding and only reforwards if during that delay no other node reforwarded the same packet (opportunistic flooding).

Compare the two protocols in terms of:
	- The percentage of nodes that actually receive the flooded data
	- The number of packets used to flood the network

Perform the comparison under different conditions in terms of: total number of nodes, distance among nodes, size of the data to distributed (from 2 bytes up to 20 bytes).

----

How to run simulations:

1) go to configs, pick up a file 'topoConfig.txt', copy it to the root, modifying:
	- the distribution of nodes (we use GRID to avoid randomness)
	- the distance between nodes (GRID_UNIT)
	- the number of nodes
2) open the terminal and type:
	java net.tinyos.sim.LinkLayerModel '/path_to_project_dir/topoConfig.txt'
3) now the files 'linkgain.out' and 'topology.out' have been created, use the same files for the two simulations with Flooding and CCBR-like (run the java command once only when the topology changes and use the same linkgain.out and topology.out for the two simulations, otherwise they are not comparable!), it is advisable to put them into a subdirectory into 'configs'
4) open 'Makefile' and uncomment the module to stress
5) open the header file for the module to stress and modify the number of bytes in the payload (0-20)
6) open the terminal and type: make micaz sim
7) edit the 'run.py' file, modifying the number of motes (must match the number specified in topoConfig.txt), the module stressed
8) open the terminal and type: ./run.py
9) collect data and cleanup directory typing: make clean
 
